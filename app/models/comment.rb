class Comment < ActiveRecord::Base
	belongs_to :post
	#needs to have post_id and body
	validates_presence_of :post_id
	validates_presence_of :body
end
