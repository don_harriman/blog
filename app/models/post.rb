class Post < ActiveRecord::Base
	has_many :comments, dependent: :destroy #when post deleted, takes all comments with it
	#needs to have title and body
	validates_presence_of :title
	validates_presence_of :body
end
